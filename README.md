# SchreibtischShowdown

This is a project build in the Unity Game Engine.

The SchreibtischShowdown repository serves as a private version management system.

# Code Rules

*1.* Always comment what the purpose of a function is.

*2.* Leave two lines between Methods

*3.* Underscore before private variables. Always have Getter and Setter available if needed.

*4.* PascalCase for Methods, camelCase for variables. The usual stuff.