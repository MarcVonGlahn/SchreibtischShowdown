using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthleteAnimationManagerArchery : AthleteAnimationManager
{
    private ArcheryAim _archeryAim;
    private ArcheryBowBehaviour _archeryBowBehaviour;
    private SceneManagerArchery _sceneManagerArchery;

    public Animator bowAnimatorController;

    //Reset Method
    public void ResetAthleteAnimationManagerArcheryValues()
    {

    }


    public void ShootSequenceTrigger()
    {

    }



    private void Awake()
    {
        SetAnimator(GetComponent<Animator>());
        _archeryBowBehaviour = FindObjectOfType<ArcheryBowBehaviour>();
        _sceneManagerArchery = FindObjectOfType<SceneManagerArchery>();
    }

    private void Update()
    {
        //After Try Skip
        if(_sceneManagerArchery.GetCurrentScenePhase() == 4)
        {
            if(Input.GetButtonDown("AT1"))
            {
                _sceneManagerArchery.SetNextScenePhase();
            }
        }
    }
}
