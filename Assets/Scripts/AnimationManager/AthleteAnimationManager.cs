﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthleteAnimationManager : MonoBehaviour
{
    private int _animationPhase = 1;
    private Animator _animator;
    private float _animationLoopSpeed;

    [Range(0.1f, 0.5f)]
    public float initialPowerAmount;

    public float runUpAnimationSpeedDevider;

    //Getter and Setter
    public int GetAnimationPhase()
    {
        return _animationPhase;
    }
    public void SetNextAnimationPhase()
    {
        int currentPhase = _animator.GetInteger("AnimationPhase");

        currentPhase++;

        _animator.SetInteger("AnimationPhase", currentPhase);
    }
    public void SetAnimationPhaseCustom(int newPhase)
    {
        int currentPhase = _animator.GetInteger("AnimationPhase");

        currentPhase = newPhase;

        _animator.SetInteger("AnimationPhase", currentPhase);
    }


    public float GetAnimationLoopSpeed()
    {
        return _animationLoopSpeed;
    }
    public void SetAnimationLoopSpeed(float currentPowerAmount)
    {

    }


    public Animator GetAnimator()
    {
        return _animator;
    }
    public void SetAnimator(Animator newAnimator)
    {
        _animator = newAnimator;
    }


    public void SetAnimatorSpeed(float newAnimationSpeed)
    {
        _animator.speed = newAnimationSpeed;
    }

    //----------------

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }


    //Is called with Animation Event to start movement of Character
    public void RunUpStartBeginMovement()
    {
        GetComponentInParent<PowerBuildUpWithTrigger>().SetPowerAmount(initialPowerAmount);
    }


    //Is called with Animation Event to reenable Input
    public void RunUpStartEndAnimation()
    {
        GetComponentInParent<PowerBuildUp>().SetInputBlocked(false);
    }


    //Controls Animation Speed for Run Up
    public void RunUpAnimationSpeed(float currentPowerAmount)
    {
        SetAnimatorSpeed(currentPowerAmount / runUpAnimationSpeedDevider);
    }
}
