﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthleteAnimationManagerPoleVault : AthleteAnimationManager
{
    private bool _riseUpMovementEnded = false;
    private bool _landingMovementEnded;


    public PoleVaultPoleBehaviour pole;


    //Getter und Setter
    public bool GetRiseUpMovementEnded()
    {
        return _riseUpMovementEnded;
    }
    public void SetRiseUpMovementEnded(bool newState)
    {
        _riseUpMovementEnded = newState;
    }


    public bool GetLandingMovementEnded()
    {
        return _landingMovementEnded;
    }
    public void SetLandingMovementEnded(bool newState)
    {
        _landingMovementEnded = newState;
    }
    //------------------------


    //Reset Method
    public void ResetAthleteAnimationManagerPoleVaultValues()
    {
        SetLandingMovementEnded(false);
        SetRiseUpMovementEnded(false);
    }


    //Sets correct PoleEnd Position and Rotation
    public void SetPoleEndInBoxPositionEvent()
    {
        pole.SetPoleEndInBoxPositionAndRotation(pole.poleGrabPointR.transform.position,
                                                pole.poleGrabPointR.transform.rotation);
    }


    //Called at the End of Lower Pole Movement Animation to Set Up change of Control
    public void LowerPoleMovementEnd()
    {
        GetComponentInParent<LaunchAngleSequence>().SetLaunchAngleSequenceActive(false);

        Time.timeScale = 1f;
        FindObjectOfType<PowerBuildUpPoleVault>().SetPowerBuildUpHappening(false);

        SetAnimatorSpeed(1f);

        pole.SwitchToRotationReferenceControl();

        Debug.Log("Lower Pole Movement Ended");
    }


    //Called at the End of Rise Up Movement Animation
    public void RiseUpMovementEnd()
    {
        SetRiseUpMovementEnded(true);
        pole.SetControlState(3);
        Debug.Log("RiseUpMovement Ended");
    }


    //Called at the End of LandingMovement
    public void LandingMovementEnd()
    {
        SetLandingMovementEnded(true);
    }


    public void EndPostTryReaction()
    {

    }
}
