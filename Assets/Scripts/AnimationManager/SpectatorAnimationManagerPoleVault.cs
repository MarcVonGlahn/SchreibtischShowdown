using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpectatorAnimationManagerPoleVault : SpectatorAnimationManager
{
    public List<GameObject> spectators;


    private void Start()
    {
        foreach(GameObject spectatorGameObject in spectators)
        {
            spectatorGameObject.GetComponent<Spectator>().StartCoroutine("RandomizeSpectatorIdleStart");
        }
    }

    
}
