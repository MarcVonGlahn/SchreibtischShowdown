using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorAnimationManager : MonoBehaviour
{
    private bool _changeAnimation;

    private int _animationPhase;


    //Getter and Setter
    public bool GetChangeAnimation()
    {
        return _changeAnimation;
    }
    public int GetAnimationPhase()
    {
        return _animationPhase;
    }
    public void SetChangeAnimation(bool newState, int newPhase)
    {
        _changeAnimation = newState;
        _animationPhase = newPhase;
    }


    //--------------------------
}
