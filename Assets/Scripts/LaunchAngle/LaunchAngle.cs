﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LaunchAngleSequence : MonoBehaviour
{
    private bool _launchAngleSequenceActive;
    private bool _launched = false;
    private float _selectedLaunchAngle;

    private PowerBuildUpWithTrigger _powerBuildUpWithTriggerObject;

    public new Rigidbody rigidbody;
    public bool launchAngleSelected { get; set; }
    private void Awake()
    {
        _powerBuildUpWithTriggerObject = GetComponent<PowerBuildUpWithTrigger>();

        launchAngleSelected = false;

    }


    //Getter und Setter
    public PowerBuildUpWithTrigger GetPowerBuildUpWithTriggerObject()
    {
        return _powerBuildUpWithTriggerObject;
    }


    public bool GetLaunchAngleSequenceActive()
    {
        return _launchAngleSequenceActive;
    }
    public void SetLaunchAngleSequenceActive(bool newState)
    {
        _launchAngleSequenceActive = newState;
    }


    public float GetSelectedLaunchAngle()
    {
        return _selectedLaunchAngle;
    }
    public void SetSelectedLaunchAngle(float newValue)
    {
        _selectedLaunchAngle = newValue*Mathf.Rad2Deg;
    }


    public bool GetLaunched()
    {
        return _launched;
    }
    public void SetLaunched(bool newState)
    {
        _launched = newState;
    }

    //----------------------------------------------------------


    //Gets Launch Angle after singular Input
    public float GetLaunchAngle(float timePassed)
    {
        float angle = 0f;

        if (Input.GetAxis("Vertical") > 0f && Input.GetAxis("Horizontal") > 0f)
        {
            angle = Mathf.Atan2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));

            if (timePassed >= 0.2f)
            {
                launchAngleSelected = true;
            }

            Debug.Log("Launch Angle: " + angle * Mathf.Rad2Deg);
        }
        return angle;
    }


    //Returns the Vector in which direction the Athlete is launched
    public Vector3 GetLaunchVector(float launchAngle)
    {
        float temporary;
        temporary = (90f-launchAngle)*Mathf.Deg2Rad;

        Debug.Log("Temporary: " + temporary);
        float a = 1;

        
        float hypotenuse = (1 / (Mathf.Sin(temporary)/**Mathf.Rad2Deg*/));

        Debug.Log("hypotenuse:" + hypotenuse);

        float b = Mathf.Sqrt((Mathf.Pow(hypotenuse, 2) - Mathf.Pow(a, 2)));

        Debug.Log("b: " + b);

        Vector3 launchVector = new Vector3(0f, (float)b, (float)-a);

        Debug.Log(launchVector.ToString("F6"));

        return launchVector;
    }


    //Launch Impulse, launches Player over Crossbar
    public void LaunchImpulse()
    {
        if (!GetLaunched())
        {
            rigidbody.AddForce( GetLaunchVector(GetSelectedLaunchAngle()).normalized * _powerBuildUpWithTriggerObject.GetPowerAmount(),
                                ForceMode.Impulse);
            rigidbody.useGravity = true;
            SetLaunched(true);
        }
    }
}
