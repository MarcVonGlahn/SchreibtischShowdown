﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchAnglePoleVault : LaunchAngleSequence
{
    public float timePassed;

    //Reset Method
    public void ResetLaunchAnglePoleVaultValues()
    {
        SetLaunched(false);
        launchAngleSelected = false;
        timePassed = 0f;
        SetSelectedLaunchAngle(0f);
    }


    //During this Sequence the Launch Angle is Selected
    public void SelectLaunchAngle()
    {
        timePassed += Time.deltaTime;
        Time.timeScale = 0.5f;

        if (!launchAngleSelected)
        {
            SetSelectedLaunchAngle(GetLaunchAngle(timePassed));
        }
    }


    private void Update()
    {
        
    }
}
