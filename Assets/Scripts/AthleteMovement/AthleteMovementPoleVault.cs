﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthleteMovementPoleVault : AthleteMovement
{
    private LevelDataControllerPoleVault _levelDataControllerPoleVault;
    private SceneManager _sceneManager;
    private PowerBuildUpPoleVault _powerBuildUpPoleVault;
    private LaunchAnglePoleVault _launchAnglePoleVault;
    private AthleteAnimationManagerPoleVault _athleteAnimationManagerPoleVault;
    private InAirControl _inAirControl;
    private PoleVaultCrossBarBehaviour _poleVaultCrossBarBehaviour;
    private SpectatorAnimationManagerPoleVault _spectatorAnimationManagerPoleVault;

    [SerializeField]
    private PoleVaultPoleBehaviour _poleVaultPoleBehaviour;

    [SerializeField]
    private Rigidbody _rigidbody;


    public Collider clearedCrossbarCollider;

    private Vector3 _tryBeginPositionAthlete;
    private Quaternion _tryBeginRotationAthlete;

    private bool _finalMovementDone = false;
    private bool _clearedCrossbar;


    //Getter und Setter
    public Vector3 GetTryBeginPositionAthlete()
    {
        return _tryBeginPositionAthlete;
    }
    public void SetTryBeginPositionAthlete(Vector3 newPosition)
    {
        _tryBeginPositionAthlete = newPosition;
    }


    public Quaternion GetTryBeginRotationAthlete()
    {
        return _tryBeginRotationAthlete;
    }
    public void SetTryBeginRotationAthlete(Quaternion newRotation)
    {
        _tryBeginRotationAthlete = newRotation;
    }


    public bool GetFinalMovementDone()
    {
        return _finalMovementDone;
    }
    public void SetFinalMovementDone(bool newState)
    {
        _finalMovementDone = newState;
    }


    public bool GetClearedCrossbar()
    {
        return _clearedCrossbar;
    }
    public void SetClearedCrossbar(bool newState)
    {
        _clearedCrossbar = newState;
    }


    //--------------------------------------


    //Reset Method
    public void ResetAthleteMovementPoleVaultValues()
    {

        SetFinalMovementDone(false);
        SetClearedCrossbar(false);

        transform.position = _tryBeginPositionAthlete;
        transform.rotation = _tryBeginRotationAthlete;
    }


    //Check if Rigidbody is Grounded after LaunchAngleHappened
    public bool IsGroundedAfterAirTime()
    {
        if(_inAirControl.GetLanded())
        {
            return true;
        }
        return false;
    }


    //Freezes Position so Character doesn't overshoots Landing Matt
    public void MaxFlyDistanceTrigger()
    {
        if(_inAirControl.MaxFlyDistanceReached())
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }



    private void Awake()
    {
        _levelDataControllerPoleVault = FindObjectOfType<LevelDataControllerPoleVault>();
        _sceneManager = FindObjectOfType<SceneManager>();
        _powerBuildUpPoleVault = GetComponent<PowerBuildUpPoleVault>();
        _launchAnglePoleVault = GetComponent<LaunchAnglePoleVault>();
        _athleteAnimationManagerPoleVault = GetComponentInChildren<AthleteAnimationManagerPoleVault>();
        _inAirControl = GetComponent<InAirControl>();
        _poleVaultCrossBarBehaviour = FindObjectOfType<PoleVaultCrossBarBehaviour>();
        _spectatorAnimationManagerPoleVault = FindObjectOfType<SpectatorAnimationManagerPoleVault>();
    }
    private void Start()
    {
        SetTryBeginPositionAthlete(transform.position);
        SetTryBeginRotationAthlete(transform.rotation);
    }


    private void Update()
    {
        if(_sceneManager.GetCurrentScenePhase() <= 3)
        {
            _poleVaultPoleBehaviour.IdleHold();
        }

        //Only, if Scene is in correct phase
        if (_sceneManager.GetCurrentScenePhase() == 4)
        {
            //PowerBuildUpPoleVault

            if (!_powerBuildUpPoleVault.GetInputBlocked())
            {
                //While Input not blocked
                if (_powerBuildUpPoleVault.GetInIdleState() == true && _powerBuildUpPoleVault.GetPowerBuildUpHappening() == false)
                {
                    _powerBuildUpPoleVault.StartRunUp();
                }
                else if (_powerBuildUpPoleVault.GetInIdleState() == false && _powerBuildUpPoleVault.GetPowerBuildUpHappening() == true)
                {
                    _powerBuildUpPoleVault.RunUpMovement();
                }
                else if (_powerBuildUpPoleVault.GetInIdleState() == false && _powerBuildUpPoleVault.GetPowerBuildUpHappening() == false)
                {

                }
            }
            else if (_powerBuildUpPoleVault.GetInputBlocked())
            {
                //While Input blocked
                _powerBuildUpPoleVault.RunUpStartMovement();
            }
            //---------------------------------------


            //LaunchAnglePoleVault

            if (_launchAnglePoleVault.GetPowerBuildUpWithTriggerObject().GetTriggerReachedState() && _launchAnglePoleVault.GetLaunchAngleSequenceActive())
            {
                _launchAnglePoleVault.SelectLaunchAngle();
            }

            //-------------------------------------------


            //PoleVaultPoleBehaviour
            if (_poleVaultPoleBehaviour.GetControlState() == 0 || _poleVaultPoleBehaviour.GetControlState() == 1)
            {
                _poleVaultPoleBehaviour.RunUpControl();
            }
            else if (_poleVaultPoleBehaviour.GetControlState() == 2)
            {
                _poleVaultPoleBehaviour.RiseRotationControl();
            }
            //When Control State 3 is reached, Pole yields all control and becomes Rigidbody

            //-----------------------------------------


            //Fly and Landing Motion
            if (!GetFinalMovementDone())
            {
                if (_poleVaultPoleBehaviour.GetControlState() > 2 && IsGroundedAfterAirTime())
                {
                    SetFinalMovementDone(true);
                    _athleteAnimationManagerPoleVault.SetNextAnimationPhase();
                }
                if (GetComponentInChildren<AthleteAnimationManagerPoleVault>().GetRiseUpMovementEnded())
                {
                    _inAirControl.EnableAllCharacterColliders();
                    _launchAnglePoleVault.LaunchImpulse();
                }
            }
            else if (GetFinalMovementDone())
            {
                _sceneManager.SetCurrentScenePhase(5);
            }

            MaxFlyDistanceTrigger();
            //---------------------------------
        }


        //Set Athlete to Post Try Position
        if (_sceneManager.GetCurrentScenePhase() == 5 && _athleteAnimationManagerPoleVault.GetLandingMovementEnded())
        {
            transform.position = _levelDataControllerPoleVault.celebrationPosition.transform.position;
            transform.rotation = _levelDataControllerPoleVault.celebrationPosition.transform.rotation;
            _sceneManager.SetCurrentScenePhase(6);

            _inAirControl.SetLanded(false);
        }


        //Trigger Celebration Animation
        if(_sceneManager.GetCurrentScenePhase() == 6 && GetClearedCrossbar())
        {
            _athleteAnimationManagerPoleVault.SetAnimationPhaseCustom(4);
            _rigidbody.useGravity = false;
            _levelDataControllerPoleVault.SetPlayerPersonalBestHeight(_poleVaultCrossBarBehaviour.GetCrossbarHeight());
        }
        else if(_sceneManager.GetCurrentScenePhase() == 6 && !GetClearedCrossbar())
        {
            _athleteAnimationManagerPoleVault.SetAnimationPhaseCustom(5);
            _rigidbody.useGravity = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other == clearedCrossbarCollider && !_poleVaultCrossBarBehaviour.GetBarKnockedOff())
        {
            SetClearedCrossbar(true);
            _spectatorAnimationManagerPoleVault.SetChangeAnimation(true, 1);
        }
        else if(other == clearedCrossbarCollider && _poleVaultCrossBarBehaviour.GetBarKnockedOff())
        {
            SetClearedCrossbar(false);
        }
    }
}
