﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerPoleVault : SceneManager
{
    private AthleteAnimationManagerPoleVault _athleteAnimationManagerPoleVault;
    private LevelDataControllerPoleVault _levelDataControllerPoleVault;
    private AIManagerPoleVault _aiManagerPoleVault;


    private void Awake()
    {
        _athleteAnimationManagerPoleVault = FindObjectOfType<AthleteAnimationManagerPoleVault>();
        _levelDataControllerPoleVault = FindObjectOfType<LevelDataControllerPoleVault>();
        _aiManagerPoleVault = FindObjectOfType<AIManagerPoleVault>();

        _aiManagerPoleVault.AIOpponentListSetMaxAchievableHeight();
        //_aiManagerPoleVault.AICalculateTry();
    }

    private void Start()
    {
        _aiManagerPoleVault.AIOpponentListSetMaxAchievableHeight();
        _aiManagerPoleVault.AICalculateTry();
    }


    private void Update()
    {
        SkipToNextPhase();
        if (GetCurrentScenePhase() == 7 && _levelDataControllerPoleVault.GetRound() == _levelDataControllerPoleVault.roundLimit)
        {
            if (Input.GetButtonDown("AT1"))
            {
                Debug.Log("Activate Next Phase");
                SetCurrentScenePhase(8);
            }
        }
        //Reset Try
        else if (GetCurrentScenePhase() == 7 && _levelDataControllerPoleVault.GetRound() < _levelDataControllerPoleVault.roundLimit)
        {
            //Show Standings for x Amount of Seconds


            if(Input.GetButtonDown("AT2"))
            {
                //Takes care of AIHeightAchieved Calculation
                _levelDataControllerPoleVault.SetRound(_levelDataControllerPoleVault.GetRound() + 1);
                _aiManagerPoleVault.AICalculateTry();
            }
            StartCoroutine(WaitForInput());
        }
    }


    private IEnumerator WaitForInput()
    {
        while (true)
        {
            yield return 0;

            if (Input.GetButtonDown("AT2"))
            {
                Debug.Log("Activate Next Phase");

                _levelDataControllerPoleVault.ResetAllPositions();
                _athleteAnimationManagerPoleVault.SetAnimationPhaseCustom(0);
                SetCurrentScenePhase(1);

                

                Debug.Log("WaitForInput For Input Ended");
                yield break;
            }
        }
        
    }
}
