using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerArchery : SceneManager
{
    private AthleteAnimationManagerArchery _athleteAnimationManagerArchery;
    private LevelDataControllerArchery _levelDataControllerArchery;
    private ArcheryAim _archeryAim;


    private void Awake()
    {
        _athleteAnimationManagerArchery = FindObjectOfType<AthleteAnimationManagerArchery>();
        _levelDataControllerArchery = FindObjectOfType<LevelDataControllerArchery>();
        _archeryAim = FindObjectOfType<ArcheryAim>();
    }


    private void Start()
    {

    }


    private void Update()
    {
        if (GetCurrentScenePhase() < 6)
        {
            SkipToNextPhase();

            if (GetCurrentScenePhase() == 2)
            {
                _archeryAim.SetAimingProcessHappening(true);
            }

            if (GetCurrentScenePhase() == 5 && _levelDataControllerArchery.GetRound() >= _levelDataControllerArchery.roundLimit)
            {
                StartCoroutine(SkipToEndDiscipline());
            }
            //Reset Try
            else if (GetCurrentScenePhase() == 5 && _levelDataControllerArchery.GetRound() < _levelDataControllerArchery.roundLimit)
            {
                //Show Standings for x Amount of Seconds

                if (Input.GetButtonDown("AT2"))
                {
                    _levelDataControllerArchery.SetRound(_levelDataControllerArchery.GetRound() + 1);
                }
                StartCoroutine(WaitForInput());
            }
        }
    }


    private IEnumerator WaitForInput()
    {
        while (true)
        {
            yield return 0;

            if (Input.GetButtonDown("AT2"))
            {
                Debug.Log("Activate Next Phase");

                _levelDataControllerArchery.ResetAllPositions();
                _athleteAnimationManagerArchery.SetAnimationPhaseCustom(0);
                SetCurrentScenePhase(1);

                Debug.Log("WaitForInput For Input Ended");
                yield break;
            }
            else if (GetCurrentScenePhase() == 6)
            {
                Debug.Log("WaitForInput ended because Discipline is ending.");
                yield break;
            }
        }
    }


    //End Discipline after final round
    private IEnumerator SkipToEndDiscipline()
    {
        while(true)
        {
            yield return 0;

            if(Input.GetButtonDown("AT1"))
            {
                SetCurrentScenePhase(6);
                Debug.Log("Discipline Ended.");
                yield break;
            }
        }
    }
}
