﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    private int _currentScenePhase;


    //Getter und Setter
    public int GetCurrentScenePhase()
    {
        return _currentScenePhase;
    }
    public void SetCurrentScenePhase(int newPhase)
    {
        _currentScenePhase = newPhase;
    }
    public void SetNextScenePhase()
    {
        _currentScenePhase++;
    }

    //----------------------

    

    public void SkipToNextPhase()
    {
        if (GetCurrentScenePhase() == 0 || GetCurrentScenePhase() == 1 || GetCurrentScenePhase() == 6)
        {
            if (Input.GetButtonDown("AT1"))
            {
                Debug.Log("Activate Next Phase");
                SetNextScenePhase();
            }
        }
    }
}
