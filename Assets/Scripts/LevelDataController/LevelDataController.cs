﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataController : MonoBehaviour
{
    private SceneManager _sceneManager;

    private string _playerName = "First Last";
    private string _playerNationality = "Universe";

    public GameObject celebrationPosition;

    //Getter und Setter
    public SceneManager GetSceneManager()
    {
        return _sceneManager;
    }

    public string GetPlayerName()
    {
        return _playerName;
    }
    public void SetPlayerName(string newPlayerName)
    {
        _playerName = newPlayerName;
    }


    public string GetPlayerNationality()
    {
        return _playerNationality;
    }
    public void SetPlayerNationality(string newPlayerNationality)
    {
        _playerNationality = newPlayerNationality;
    }
    //----------------

    private void Awake()
    {
        _sceneManager = FindObjectOfType<SceneManager>();
    }
}
