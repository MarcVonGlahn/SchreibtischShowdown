using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataControllerArchery : LevelDataController
{
    private AthleteAnimationManagerArchery _athleteAnimationManagerArchery;
    private ArcheryAim _archeryAim;

    private int _currentPointTotal;

    private int _round = 1;

    public int roundLimit;


    //Getter und Setter
    public int GetCurrentPointTotal()
    {
        return _currentPointTotal;
    }
    public void SetCurrentPointTotal(int newValue)
    {
        _currentPointTotal += newValue;
    }


    public int GetRound()
    {
        return _round;
    }
    public void SetRound(int newValue)
    {
        _round = newValue;
    }
    //-----------------------

    //Reset all Elements of Archery Scene before a new try
    public void ResetAllPositions()
    {

    }



    private void Awake()
    {
        _athleteAnimationManagerArchery = FindObjectOfType<AthleteAnimationManagerArchery>();
        _archeryAim = FindObjectOfType<ArcheryAim>();
    }

    private void Start()
    {

    }


    private void Update()
    {

    }
}
