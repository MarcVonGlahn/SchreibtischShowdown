﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataControllerPoleVault : LevelDataController
{
    private AthleteAnimationManagerPoleVault _athleteAnimationManagerPoleVault;
    private AthleteMovementPoleVault _athleteMovementPoleVault;

    private InAirControl _inAirControl;

    private LaunchAnglePoleVault _launchAnglePoleVault;

    private PoleVaultCrossBarBehaviour _poleVaultCrossBarBehaviour;
    private PoleVaultPoleBehaviour _poleVaultPoleBehaviour;

    private PowerBuildUpPoleVault _powerBuildUpPoleVault;


    private Vector3 _tryBeginPositionAthlete;
    private Quaternion _tryBeginRotationAthlete;

    
    private float _lastSavedHeight;

    private float _playerPersonalBestHeight;

    private int _round = 1;

    public int roundLimit;


    //Getter und Setter
    public float GetLastSavedHeight()
    {
        return _lastSavedHeight;
    }
    public void SetLastSavedHeight(float newHeight)
    {
        _lastSavedHeight = newHeight;
    }


    public float GetPlayerPersonalBestHeight()
    {
        return _playerPersonalBestHeight;
    }
    public void SetPlayerPersonalBestHeight(float newHeight)
    {
        newHeight = Mathf.Round(newHeight * 100f) / 100f;
        _playerPersonalBestHeight = newHeight;
    }


    public int GetRound()
    {
        return _round;
    }
    public void SetRound(int newValue)
    {
        _round = newValue;
    }
    //-----------------------

    //Reset all Elements of Pole Vault Scene before a new try
    public void ResetAllPositions()
    {
        _athleteMovementPoleVault.ResetAthleteMovementPoleVaultValues();

        _powerBuildUpPoleVault.ResetPowerBuildUpPoleVaultValues();

        _poleVaultCrossBarBehaviour.ResetPoleVaultCrossbarBehaviourValues();
        _poleVaultCrossBarBehaviour.SetCrossbarHeight(_lastSavedHeight);

        _poleVaultPoleBehaviour.ResetPoleVaultPoleBehaviourValues();

        _athleteAnimationManagerPoleVault.ResetAthleteAnimationManagerPoleVaultValues();

        _launchAnglePoleVault.ResetLaunchAnglePoleVaultValues();

        _inAirControl.DisableAllCharacterColliders();
    }



    private void Awake()
    {
        _athleteAnimationManagerPoleVault = FindObjectOfType<AthleteAnimationManagerPoleVault>();
        _athleteMovementPoleVault = FindObjectOfType<AthleteMovementPoleVault>();
        _inAirControl = FindObjectOfType<InAirControl>();
        _launchAnglePoleVault = FindObjectOfType<LaunchAnglePoleVault>();
        _powerBuildUpPoleVault = FindObjectOfType<PowerBuildUpPoleVault>();
        _poleVaultCrossBarBehaviour = FindObjectOfType<PoleVaultCrossBarBehaviour>();
        _poleVaultPoleBehaviour = FindObjectOfType<PoleVaultPoleBehaviour>();

        
    }

    private void Start()
    {
    }


    private void Update()
    {
    }
}
