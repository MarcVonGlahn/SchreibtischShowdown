﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBuildUpPoleVault : PowerBuildUpWithTrigger
{
    public void ResetPowerBuildUpPoleVaultValues()
    {
        SetInIdleState(true);
        SetPowerBuildUpHappening(false);
        SetTriggerReachedState(false);
        SetPowerAmount(0f);
    }


    private void Awake()
    {
        SetTransform(this.GetComponent<Transform>());
    }


    public override float MeasureDistanceLeft()
    {
        float placeHolder = 0;
        return placeHolder;
    }


    //Starts the Run Up Phase with the press of "AT1"
    public override void StartRunUp()
    {
        if (Input.GetButtonDown("AT1"))
        {
            SetInIdleState(false);
            SetPowerBuildUpHappening(true);

            GetAnimationManager().SetNextAnimationPhase();

            //Block Input
            SetInputBlocked(true);

            pole.SetControlState(1);

            Debug.Log("Start Run Up Phase");
        }
    }


    public override void RunUpMovement()
    {
        PowerBuildUpMethod();
        transform.Translate(0, (GetPowerAmount()) * Time.deltaTime, 0, runUpOrientation.transform);
    }
}
