﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class PowerBuildUpWithTrigger : PowerBuildUp
{
    public float buildUpMultiplier;

    [Range(0.01f, 0.1f)]
    public float buildUpSubtractor;

    private bool _inIdle = true;
    private bool _triggerReached = false;
    private float _distanceToBuildUpEnd;
    private float _distanceTravelled;
    private Transform _transform;


    private Vector3 _vectorRunUpOrientation;


    public PoleVaultPoleBehaviour pole;


    public GameObject runUpOrientation;


    public Collider angleAimTrigger;
    public Collider lowerPoleTrigger;


    public Animator animator;


    //Getter and Setter Methods
    public bool GetInIdleState()
    {
        return _inIdle;
    }
    public void SetInIdleState(bool newState)
    {
        _inIdle = newState;
    }

    
    public bool GetTriggerReachedState()
    {
        return _triggerReached;
    }
    public void SetTriggerReachedState(bool newState)
    {
        _triggerReached = newState;
    }


    public Vector3 GetRunUpOrientation()
    {
        return _vectorRunUpOrientation;
    }


    public Transform GetTransform()
    {
        return _transform;
    }
    public void SetTransform(Transform newTransform)
    {
        _transform = newTransform;
    }


    //---------------------------
    private void Awake()
    {
        _vectorRunUpOrientation = this.transform.position - runUpOrientation.transform.position;
    }


    //Still experimental, might be removed due to unusability
    public abstract float MeasureDistanceLeft();


    //Run Up Movement when Input is NOT blocked
    public abstract void RunUpMovement();


    //Starts the Run Up Phase with the press of "AT1" 
    public abstract void StartRunUp();


    //This is the Movement when Input is blocked
    public void RunUpStartMovement()
    {
        transform.Translate(0, (GetPowerAmount()) * Time.deltaTime, 0, runUpOrientation.transform);
    }


    //This is the PowerBuildUp during the Run Up
    public override void PowerBuildUpMethod()
    {
        if (!GetTriggerReachedState())
        {
            if (Input.GetButtonDown("AT1"))
            {
                AddToPowerAmount(buildUpMultiplier);
                Debug.Log(GetPowerAmount());
            }
        }
        if (GetPowerAmount() > GetAnimationManager().initialPowerAmount && GetTriggerReachedState() == false)
        {
            AddToPowerAmount(-(buildUpSubtractor));
        }
        GetAnimationManager().RunUpAnimationSpeed(GetPowerAmount());
    }


    //Trigger Methods
    private void OnTriggerEnter(Collider other)
    {
        if(other == angleAimTrigger)
        {
            SetTriggerReachedState(true);

            GetComponent<LaunchAngleSequence>().SetLaunchAngleSequenceActive(true);

            Debug.Log("Angle Aim Trigger activated");
        }
        else if (other == lowerPoleTrigger)
        {
            GetAnimationManager().SetAnimationPhaseCustom(2);

            //SetNextPhase();

            Debug.Log("Lower Pole Trigger activated");
        }
    }
}
