﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class PowerBuildUp : MonoBehaviour
{
    private bool _powerBuildUpHappening = false;

    private bool _inputBlocked = false;

    private float _powerAmount;

    private AthleteAnimationManager _animationManager;


    //Getter and Setter Methods ----------------

    public bool GetPowerBuildUpHappening()
    {
        return _powerBuildUpHappening;
    }
    public void SetPowerBuildUpHappening(bool newState)
    {
        _powerBuildUpHappening = newState;
    }


    public bool GetInputBlocked()
    {
        return _inputBlocked;
    }
    public void SetInputBlocked(bool newState)
    {
        _inputBlocked = newState;
    }


    public float GetPowerAmount()
    {
        return _powerAmount;
    }
    public void AddToPowerAmount(float powerAmountAdded)
    {
        _powerAmount += powerAmountAdded;
    }
    public void SetPowerAmount(float newValue)
    {
        _powerAmount = newValue;
    }


    public AthleteAnimationManager GetAnimationManager()
    {
        return _animationManager;
    }


    //-------------------------------

    private void Start()
    {
        _animationManager = GetComponentInChildren<AthleteAnimationManager>();
    }


    //Builds up Power through repeated pressing of designated Button which is declared in impelementation of Button
    public abstract void PowerBuildUpMethod();
}
