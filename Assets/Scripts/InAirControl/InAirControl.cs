﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InAirControl : MonoBehaviour
{
    private SceneManager _sceneManager;
    private bool _landed = false;

    public GameObject maxFlyDistanceReference;
    public Collider landingMattCollider;


    public List<Collider> characterColliders;


    //Getter und Setter
    public bool GetLanded()
    {
        return _landed;
    }
    public void SetLanded(bool newState)
    {
        _landed = newState;
    }


    //-----------------------


    private void Start()
    {
        _sceneManager = FindObjectOfType<SceneManager>();


        //Disable all Character Collider at Beginning
        foreach(Collider characterCollider in characterColliders)
        {
            characterCollider.enabled = false;
        }
    }

    public bool MaxFlyDistanceReached()
    {
        if(transform.position.z <= maxFlyDistanceReference.transform.position.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    //Enable All Character Colliders at the end of RiseUpMovement
    public void EnableAllCharacterColliders()
    {
        foreach (Collider characterCollider in characterColliders)
        {
            characterCollider.enabled = true;
        }
    }
    public void DisableAllCharacterColliders()
    {
        foreach (Collider characterCollider in characterColliders)
        {
            characterCollider.enabled = false;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other == landingMattCollider)
        {
            SetLanded(true);
        }
    }
}
