﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIManagerPoleVault : UIManager
{
    private AIManagerPoleVault _aiManagerPoleVault;
    private PowerBuildUpPoleVault _powerBuildUpPoleVault;
    private PoleVaultCrossBarBehaviour _crossBarBehaviour;
    private LaunchAnglePoleVault _launchAnglePoleVault;
    private LevelDataControllerPoleVault _levelDataControllerPoleVault;

    private float _uiPowerAmount;
    private List<GameObject> _powerBuildUpBarList;

    public Canvas introCanvas;
    public Canvas idleCanvas;
    public Canvas heightChoosingCanvas;
    public Canvas tryCanvas;
    public Canvas postTryCanvas;
    public Canvas overviewCanvas;

    private bool _scoreboardUpdated;
    private UITextScoreboard playerScoreboardElement;
    private List<UITextScoreboard> _scoreboardElements;
    private List<GameObject> _scoreboardSpots;
    public GameObject scoreboardIdleParent;
    public GameObject scoreboardOverviewParent;
    public GameObject scoreBoardTopReference;
    public GameObject scoreboardPlayerReference;
    public int scoreboardSpotOffset;

    public TextMeshProUGUI crossbarHeightText;

    public GameObject powerBuildUpTemplate;
    public GameObject powerBuildUpBarParent;
    public GameObject powerBuildUpBar;

    public GameObject launchAngleTemplate;
    public GameObject launchAngleArrow;
    public TextMeshProUGUI launchAngleText;




    //Getter und Setter
    public void SetLaunchAngleTemplateActive(bool newState)
    {
        launchAngleTemplate.SetActive(newState);
    }


    public void SetPowerBuildUpTemplateActive(bool newState)
    {
        powerBuildUpTemplate.SetActive(newState);
    }

    //-----------------------


    //Controls the HeightChoosing Canvas
    public void HeightChoosingCanvasUpdate()
    {
        crossbarHeightText.SetText(_crossBarBehaviour.GetCrossbarHeight().ToString("0.00") + " m");
    }


    //PowerBuildUp Bar Progress
    public void PowerBuildUpBarUpdate()
    {
        _uiPowerAmount = _powerBuildUpPoleVault.GetPowerAmount();
        _uiPowerAmount *= 3;
        foreach (GameObject bar in _powerBuildUpBarList)
        {
            bar.SetActive(false);
        }
        for (int i = 0; i < (int)_uiPowerAmount; i++)
        {
            _powerBuildUpBarList[i].SetActive(true);
        }

    }


    //Controls the LaunchAngleArrow
    public void LaunchAngleArrowUpdate()
    {
        if (!_launchAnglePoleVault.launchAngleSelected)
        {
            launchAngleArrow.transform.eulerAngles = new Vector3(0, 0, _launchAnglePoleVault.GetSelectedLaunchAngle());
            launchAngleText.SetText(_launchAnglePoleVault.GetSelectedLaunchAngle().ToString("0.0") + "°");
        }
    }


    private void Start()
    {

        List<Canvas> canvasList = new List<Canvas>();

        canvasList.Add(introCanvas);
        canvasList.Add(idleCanvas);
        canvasList.Add(heightChoosingCanvas);
        canvasList.Add(tryCanvas);
        canvasList.Add(postTryCanvas);
        canvasList.Add(overviewCanvas);

        foreach (Canvas canvas in canvasList)
        {
            canvas.gameObject.SetActive(false);
        }


        _aiManagerPoleVault = FindObjectOfType<AIManagerPoleVault>();
        _powerBuildUpPoleVault = FindObjectOfType<PowerBuildUpPoleVault>();
        _crossBarBehaviour = FindObjectOfType<PoleVaultCrossBarBehaviour>();
        _launchAnglePoleVault = FindObjectOfType<LaunchAnglePoleVault>();
        _levelDataControllerPoleVault = FindObjectOfType<LevelDataControllerPoleVault>();
        SetLaunchAngleTemplateActive(false);


        //AI Opponent List Instantiation

        idleCanvas.gameObject.SetActive(true);

        List<GameObject> scoreboardSpots = new List<GameObject>();

        for (int i = 0; i < _aiManagerPoleVault.aiOpponents.Count; i++)
        {
            scoreboardSpots.Add(Instantiate(scoreBoardTopReference, new Vector3(scoreBoardTopReference.transform.position.x, scoreBoardTopReference.transform.position.y - i * scoreboardSpotOffset, 0f),
                                                                    new Quaternion(0f, 0f, 0f, 0f)));
        }

        _scoreboardSpots = scoreboardSpots;

        foreach (GameObject spot in _scoreboardSpots)
        {
            spot.transform.SetParent(scoreboardIdleParent.transform);
        }


        //AIOpponentsTextElements Initialisation

        List<UITextScoreboard> _UITextScoreboardsDynamic = new List<UITextScoreboard>();

        for(int i = 0; i < _aiManagerPoleVault.aiOpponents.Count; i++)
        {
            _UITextScoreboardsDynamic.Add(Instantiate(_aiManagerPoleVault.aiOpponents[i].uiTextScoreboard, 
                                                                    new Vector3(_scoreboardSpots[i].transform.position.x, _scoreboardSpots[i].transform.position.y, 0f), 
                                                                    new Quaternion(0f, 0f, 0f, 0f)));
        }


        _scoreboardElements = _UITextScoreboardsDynamic;

        foreach(UITextScoreboard scoreboardElement in _scoreboardElements)
        {
            scoreboardElement.transform.SetParent(scoreboardIdleParent.transform);
        }

        idleCanvas.gameObject.SetActive(false);


        //Player Scoreboard Spot Initialisation

        UITextScoreboard dynamicScoreboard = new UITextScoreboard();

        dynamicScoreboard = Instantiate(_aiManagerPoleVault.aiOpponents[0].uiTextScoreboard, new Vector3(scoreboardPlayerReference.transform.position.x, scoreboardPlayerReference.transform.position.y, 0f), new Quaternion(0f, 0f, 0f, 0f));

        playerScoreboardElement = dynamicScoreboard;
        playerScoreboardElement.transform.SetParent(scoreboardIdleParent.transform);

        playerScoreboardElement.name.SetText(_levelDataControllerPoleVault.GetPlayerName());
        playerScoreboardElement.nationality.SetText(_levelDataControllerPoleVault.GetPlayerNationality());
        playerScoreboardElement.height.SetText(_levelDataControllerPoleVault.GetPlayerPersonalBestHeight().ToString("0.00") + " m");

        //Instantiate Elements for PowerBuildUp Progress Bar

        tryCanvas.gameObject.SetActive(true);

        List<GameObject> _randomList = new List<GameObject>();
        for (int i = 0; i < 35; i++)
        {
            _randomList.Add(Instantiate(powerBuildUpBar, new Vector3((powerBuildUpBar.transform.position.x + i * 10), powerBuildUpBar.transform.position.y, 0f), new Quaternion(0f, 0f, 0f, 0f)));
        }

        _powerBuildUpBarList = _randomList;

        foreach (GameObject bar in _powerBuildUpBarList)
        {
            bar.transform.SetParent(powerBuildUpBarParent.transform);
        }

        tryCanvas.gameObject.SetActive(false);
    }


    //Scoreboard Update function
    public void ScoreboardUpdate()
    {
        for (int i = 0; i < _aiManagerPoleVault.aiOpponents.Count; i++)
        {
            _scoreboardElements[i].name.SetText(_aiManagerPoleVault.aiOpponents[i].name);
            _scoreboardElements[i].nationality.SetText(_aiManagerPoleVault.aiOpponents[i].nationality);
            _scoreboardElements[i].height.SetText(_aiManagerPoleVault.aiOpponents[i].GetAchievedMaxHeight().ToString("0.00") + " m");

            playerScoreboardElement.name.SetText(_levelDataControllerPoleVault.GetPlayerName());
            playerScoreboardElement.nationality.SetText(_levelDataControllerPoleVault.GetPlayerNationality());
            playerScoreboardElement.height.SetText(_levelDataControllerPoleVault.GetPlayerPersonalBestHeight().ToString("0.00") + " m");
        }

        
    }


    public void ScoreboardChangeParent(Transform newParent)
    {
        foreach(UITextScoreboard scoreBoardElement in _scoreboardElements)
        {
            scoreBoardElement.transform.SetParent(newParent);
        }

        playerScoreboardElement.transform.SetParent(newParent);
    }


    void Update()
    {
        if (GetSceneManager().GetCurrentScenePhase() == 0)
        {
            SetCanvas(introCanvas);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 1)
        {
            SetCanvas(idleCanvas);
            ScoreboardChangeParent(scoreboardIdleParent.transform);
            if(switchedToNewCanvas)
            {
                ScoreboardUpdate();
            }
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 2 ||
                    GetSceneManager().GetCurrentScenePhase() == 3)
        {
            SetCanvas(heightChoosingCanvas);
            HeightChoosingCanvasUpdate();
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 4)
        {
            SetCanvas(tryCanvas);
            if (_launchAnglePoleVault.GetLaunchAngleSequenceActive())
            {
                SetPowerBuildUpTemplateActive(false);

                SetLaunchAngleTemplateActive(true);
                LaunchAngleArrowUpdate();
            }
            else if (!_launchAnglePoleVault.GetPowerBuildUpWithTriggerObject().GetTriggerReachedState())
            {
                SetPowerBuildUpTemplateActive(true);
                PowerBuildUpBarUpdate();

                SetLaunchAngleTemplateActive(false);
            }
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 5)
        {
            SetCanvas(postTryCanvas);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 7)
        {
            SetCanvas(overviewCanvas);
            ScoreboardChangeParent(scoreboardOverviewParent.transform);
            if(switchedToNewCanvas)
            {
                ScoreboardUpdate();
            }
        }
    }  
}
