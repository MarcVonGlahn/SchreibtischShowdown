using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UITextScoreboard : MonoBehaviour
{
    public TextMeshProUGUI name;
    public TextMeshProUGUI nationality;
    public TextMeshProUGUI height;
}
