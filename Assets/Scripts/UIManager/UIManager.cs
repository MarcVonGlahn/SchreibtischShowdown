﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    private SceneManager _sceneManager;
    protected CameraManager _cameraManager;
    private Canvas _activeCanvas;

    public Animator transition;

    public bool switchedToNewCanvas;


    //Getter und Setter
    public SceneManager GetSceneManager()
    {
        return _sceneManager;
    }


    public Canvas GetActiveCanvas()
    {
        return _activeCanvas;
    }

    public void SetActiveCanvasState(bool newState)
    {
        _activeCanvas.gameObject.SetActive(newState);
    }
    //------------------------


    private void Awake()
    {
        _sceneManager = FindObjectOfType<SceneManager>();
        _cameraManager = FindObjectOfType<CameraManager>();
    }


    public void SetCanvas(Canvas nextCanvas)
    {
        if (_activeCanvas == null)
        {
            _activeCanvas = nextCanvas;
            _activeCanvas.gameObject.SetActive(true);
            switchedToNewCanvas = true;
        }
        else if (_activeCanvas == nextCanvas)
        {
            switchedToNewCanvas = false;
        }
        else
        {
            switchedToNewCanvas = true;
            _activeCanvas.gameObject.SetActive(false);
            nextCanvas.gameObject.SetActive(true);
            _activeCanvas = nextCanvas;
        }
    }
}
