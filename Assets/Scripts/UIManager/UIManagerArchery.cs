using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManagerArchery : UIManager
{
    private SceneManagerArchery _sceneManagerArchery;
    private WeatherControl _weatherControl;

    public GameObject windArrow;

    public Canvas introCanvas;
    public Canvas idleCanvas;
    public Canvas aimCanvas;
    public Canvas postTryCanvas;
    public Canvas overviewCanvas;


    private void Awake()
    {
        _weatherControl = FindObjectOfType<WeatherControl>();
        _sceneManagerArchery = FindObjectOfType<SceneManagerArchery>();
    }


    //Updates the UI WindDirectionArrow Element
    public void WindDirectionArrowUpdate()
    {
        windArrow.transform.eulerAngles = new Vector3(0f, 0f, _weatherControl.GetWindAngle());
    }


    // Update is called once per frame
    void Update()
    {
        if(_sceneManagerArchery.GetCurrentScenePhase() == 2)
        {
            WindDirectionArrowUpdate();
        }
    }
}
