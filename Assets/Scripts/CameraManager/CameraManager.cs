﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private SceneManager _sceneManager;
    private Camera _activeCamera;
    private UIManager _UIManager;

    private bool _transitionHappening;

    public Animator transition;

    //Make sure all Camera GameObjects are Inactive at beginning
    public Camera introCamera;
    public Camera idleCamera;
    public Camera athleteCamera;
    public Camera postTryCamera;
    public Camera overviewCamera;
    public Camera postDisciplineCamera;

    //Getter und Setter
    public Camera GetActiveCamera()
    {
        return _activeCamera;
    }
    public void SetActiveCamera(Camera nextCamera)
    {
        if(_activeCamera == null)
        {
            _activeCamera = nextCamera;
            _activeCamera.gameObject.SetActive(true);
        }
        else if(_activeCamera == nextCamera)
        {
            
        }
        else if(_activeCamera != null)
        {
            _activeCamera.gameObject.SetActive(false);
            nextCamera.gameObject.SetActive(true);
            _activeCamera = nextCamera;
        }
    }


    public SceneManager GetSceneManager()
    {
        return _sceneManager;
    }


    public bool GetTransitionHappening()
    {
        return _transitionHappening;
    }
    public void SetTransitionHappening(bool newState)
    {
        _transitionHappening = newState;
    }

    //------------------


    private void Awake()
    {
        _sceneManager = FindObjectOfType<SceneManager>();
        SetActiveCamera(introCamera);
    }
}
