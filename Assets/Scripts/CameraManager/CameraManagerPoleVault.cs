﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagerPoleVault : CameraManager
{
    private UIManagerPoleVault _uiManagerPoleVault;
    public Camera heightChoosingCamera;


    private void Update()
    {
        if (GetSceneManager().GetCurrentScenePhase() == 0)
        {
            SetActiveCamera(introCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 1 ||
                    GetSceneManager().GetCurrentScenePhase() == 2)
        {
            SetActiveCamera(idleCamera);
        }
        else if(GetSceneManager().GetCurrentScenePhase() == 3)
        {
            SetActiveCamera(heightChoosingCamera);
        }
        else if(GetSceneManager().GetCurrentScenePhase() == 4)
        {
            SetActiveCamera(athleteCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 6)
        {
            SetActiveCamera(postTryCamera);
        }
        else if(GetSceneManager().GetCurrentScenePhase() == 7)
        {
            SetActiveCamera(overviewCamera);
        }
    }
}
