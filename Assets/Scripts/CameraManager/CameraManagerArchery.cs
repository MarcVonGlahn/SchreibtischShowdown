using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagerArchery : CameraManager
{
    public Camera aimCamera;
    public Camera arrowFollowCamera;


    void Update()
    {
        if (GetSceneManager().GetCurrentScenePhase() == 0)
        {
            SetActiveCamera(introCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 1)
        {
            SetActiveCamera(idleCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 2)
        {
            SetActiveCamera(aimCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 3)
        {
            SetActiveCamera(arrowFollowCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 4)
        {
            SetActiveCamera(postTryCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 5)
        {
            SetActiveCamera(overviewCamera);
        }
        else if (GetSceneManager().GetCurrentScenePhase() == 6)
        {
            SetActiveCamera(postDisciplineCamera);
        }
    }
}
