using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIOpponentPoleVault : AIOpponent
{
    private float _achievedMaxHeight;
    private float _maxPossibleAchievableHeight;


    public UITextScoreboard uiTextScoreboard;


    //Getter and Setter
    public float GetAchievedMaxHeight()
    {
        return _achievedMaxHeight;
    }
    public void SetAchievedMaxHeight(float newValue)
    {
        _achievedMaxHeight = newValue;
    }


    public float GetMaxPossibleAchievableHeight()
    {
        return _maxPossibleAchievableHeight;
    }
    public void SetMaxPossibleAchievableHeight(float newValue)
    {
        _maxPossibleAchievableHeight = newValue;
    }
    //--------------


}
