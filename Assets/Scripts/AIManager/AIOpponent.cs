using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class AIOpponent
{
    public int oppID;
    public string name;
    public string nationality;
    public int abilityValue;
}
