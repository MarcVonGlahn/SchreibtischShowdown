using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManagerPoleVault : AIManager
{
    private LevelDataControllerPoleVault _levelDataControllerPoleVault;


    private float _achievedMaxHeight;
    private bool _aiClearedCrossbar;
    private float _aiSelectedHeight;


    public float maxHeightPossible;
    public float lowestHeightPossible;

    public List<AIOpponentPoleVault> aiOpponents = new List<AIOpponentPoleVault>();

    //Getter and Setter
    public float GetAchievedMaxHeight()
    {
        return _achievedMaxHeight;
    }
    public void SetAchievedMaxHeight(float newMaxHeight)
    {
        _achievedMaxHeight = newMaxHeight;
    }


    public bool GetAIClearedCrossbar()
    {
        return _aiClearedCrossbar;
    }
    public void SetAIClearedCrossbar(bool newState)
    {
        _aiClearedCrossbar = newState;
    }


    public float GetAISelectedHeight()
    {
        return _aiSelectedHeight;
    }
    public void SetAISelectedHeight(float newValue)
    {
        _aiSelectedHeight = newValue;
    }
    //-----------------------

    private void Awake()
    {
        _levelDataControllerPoleVault = FindObjectOfType<LevelDataControllerPoleVault>();
    }

    public void AIOpponentListSetMaxAchievableHeight()
    {
        foreach(AIOpponentPoleVault aiOpponent in aiOpponents)
        {
            float maxAchievableHeight = (maxHeightPossible-lowestHeightPossible) * (aiOpponent.abilityValue / 100f);
            maxAchievableHeight += lowestHeightPossible;

            aiOpponent.SetMaxPossibleAchievableHeight(maxAchievableHeight);
        }
    }


    public void SwapScoreBoardPosition(AIOpponentPoleVault item1, AIOpponentPoleVault item2)
    {
        AIOpponentPoleVault temp;

        temp = item1;

        item1 = item2;

        item2 = temp;
    }


    public List<AIOpponentPoleVault> SortByHeight(List<AIOpponentPoleVault> originalList)
    {
        int i, j, imin;
        for (i = 0; i < originalList.Count - 1; i++)
        {
            imin = i;

            for (j = i + 1; j < originalList.Count; j++)
            {
                if(originalList[j].GetAchievedMaxHeight() > originalList[imin].GetAchievedMaxHeight())
                {
                    imin = j;
                }
            }

            AIOpponentPoleVault temp = originalList[i];

            originalList[i] = originalList[imin];

            originalList[imin] = temp;
        }

        return originalList;
    }

    //Calculate the achieved Height for the AI on each try
    public void AICalculateTry()
    {
        foreach(AIOpponentPoleVault aiOpponent in aiOpponents)
        {
            if (_levelDataControllerPoleVault.GetRound() == 1)
            {
                SetAISelectedHeight(lowestHeightPossible);
                aiOpponent.SetAchievedMaxHeight(GetAISelectedHeight());
            }
            else if (_levelDataControllerPoleVault.GetRound() <= _levelDataControllerPoleVault.roundLimit)
            {
                SetAISelectedHeight(aiOpponent.GetAchievedMaxHeight());
                int rand = Random.Range(1, 10);

                if(GetAISelectedHeight() + (rand * 0.04f) <= aiOpponent.GetMaxPossibleAchievableHeight())
                {
                    SetAISelectedHeight(GetAISelectedHeight() + (rand * 0.04f));
                    aiOpponent.SetAchievedMaxHeight(GetAISelectedHeight());
                }
                else
                {
                    aiOpponent.SetAchievedMaxHeight(GetAISelectedHeight());
                }
            }
        }
        aiOpponents = SortByHeight(aiOpponents);
    }


    
}
