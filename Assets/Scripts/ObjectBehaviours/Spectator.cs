using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spectator: MonoBehaviour
{
    private AthleteMovementPoleVault _athleteMovementPoleVault;

    private Animator _animator;

    private bool _idleStarted;

    private void Start()
    {
        _athleteMovementPoleVault = FindObjectOfType<AthleteMovementPoleVault>();
        _animator = GetComponent<Animator>();

        //StartCoroutine(RandomizeSpectatorIdleStart());
    }


    private void Update()
    {
        if(!_idleStarted)
        {
            //StartCoroutine(RandomizeSpectatorIdleStart());
        }
    }

    private IEnumerator RandomizeSpectatorIdleStart()
    {
        float rand = Random.Range(0.4f, 1.5f);

        yield return new WaitForSeconds(rand);

        _animator.SetTrigger("CelebrationTrigger");

        _idleStarted = true;
    }



}
