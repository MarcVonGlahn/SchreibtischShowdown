using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcheryArrowBehaviour : MonoBehaviour
{
    private SceneManagerArchery _sceneManagerArchery;
    private LevelDataControllerArchery _levelDataControllerArchery;
    private ArcheryAim _archeryAim;
    private Vector3 _arrowStartToAim;

    private bool _arrowFlying;


    public GameObject arrowReference;
    public GameObject aimReference;

    public float arrowFlyingSpeed;
    public float arrowStuckInTargetOffset;

    //Getter and Setter
    public bool GetArrowFlying()
    {
        return _arrowFlying;
    }
    public void SetArrowFlying(bool newState)
    {
        _arrowFlying = newState;
    }

    //------------------


    public void ResetArrowPosition()
    {
        transform.position = arrowReference.transform.position;
    }


    //Sets the path the arrow has to travel to get to the target
    public void SetArrowPath()
    {
        _arrowStartToAim = arrowReference.transform.position - _archeryAim.archeryAimReference.transform.position;
    }


    //Directs the arrow to the Target
    public void FlyOntoTarget(Vector3 targetPosition)
    {
        if ((aimReference.transform.position.z + arrowStuckInTargetOffset) < transform.position.z
                && GetArrowFlying() == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, arrowFlyingSpeed);
            Debug.Log("Arrow is Moving");
        }
        else if ((aimReference.transform.position.z + +arrowStuckInTargetOffset) >= transform.position.z 
                && GetArrowFlying() == true)
        {
            SetArrowFlying(false);
        }
    }

    private void Awake()
    {
        _archeryAim = FindObjectOfType<ArcheryAim>();
        _levelDataControllerArchery = FindObjectOfType<LevelDataControllerArchery>();
        _sceneManagerArchery = FindObjectOfType<SceneManagerArchery>();
    }

    void Start()
    {
        ResetArrowPosition();
    }


    private void Update()
    {
        if (_sceneManagerArchery.GetCurrentScenePhase() == 3 && GetArrowFlying() == true)
        {
            FlyOntoTarget(aimReference.transform.position + new Vector3(0f, 0f, arrowStuckInTargetOffset));
        }
        else if(_sceneManagerArchery.GetCurrentScenePhase() == 3 && GetArrowFlying() == false)
        {
            StartCoroutine(WaitToConinue());
        }
        else if(_sceneManagerArchery.GetCurrentScenePhase() < 3 && GetArrowFlying() == false)
        {
            transform.position = arrowReference.transform.position;
        }
    }



    public IEnumerator WaitToConinue()
    {
        if(Input.GetButtonDown("AT1"))
        {
            _levelDataControllerArchery.SetCurrentPointTotal(_archeryAim.GetScoreDecision());
            _sceneManagerArchery.SetNextScenePhase();
            yield return 0;
        }
    }
}
