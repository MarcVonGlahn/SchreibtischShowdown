﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoleVaultPoleBehaviour : MonoBehaviour
{
    private int _controlState = 0;
    private float _riseRotationValue = 0;

    private Rigidbody _poleRigidbody;
    private Collider _poleCollider;

    private Vector3 _poleAthleteOffset;
    private Quaternion _poleAthleteRotationOffset;
    private Vector3 _poleEndInBoxPosition;
    private Quaternion _poleEndInBoxRotation;

    private Quaternion _tryBeginRotationPole;


    public GameObject handR;
    public GameObject handL;

    public GameObject poleGrabPointR;
    public GameObject pole;
    public GameObject poleEnd;
    public GameObject rotationReference;

    public GameObject athlete;
    public GameObject armature;

    [Range(10.0f, 90.0f)]
    public float riseRotationSpeed;

    public float riseRotationLimit;


    //Getter und Setter
    public int GetControlState()
    {
        return _controlState;
    }
    public void SetControlState(int newState)
    {
        _controlState = newState;
    }


    public float GetRiseRotationValue()
    {
        return _riseRotationValue;
    }
    public void SetRiseRotationValue(float newValue)
    {
        _riseRotationValue = newValue;
    }


    public Vector3 GetPoleAthleteOffset()
    {
        return _poleAthleteOffset;
    }
    public void SetPoleAthleteOffset(Vector3 newValue)
    {
        _poleAthleteOffset = newValue;
    }


    public Vector3 GetPoleEndInBoxPosition()
    {
        return _poleEndInBoxPosition;
    }
    public void SetPoleEndInBoxPositionAndRotation(Vector3 newPosition, Quaternion newRotation)
    {
        _poleEndInBoxPosition = newPosition;
        _poleEndInBoxRotation = newRotation;
    }


    public Quaternion GetTryBeginRotationPole()
    {
        return _tryBeginRotationPole;
    }
    public void SetTryBeginRotationPole(Quaternion newRotation)
    {
        _tryBeginRotationPole = newRotation;
    }

    //-------------------------------------



    private void Awake()
    {
        _poleCollider = GetComponentInChildren<Collider>();
        _poleRigidbody = GetComponentInChildren<Rigidbody>();
    }

    //ResetMethod
    public void ResetPoleVaultPoleBehaviourValues()
    {
        SetControlState(0);
        SetPoleEndInBoxPositionAndRotation(new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
        poleEnd.transform.rotation = GetTryBeginRotationPole();
        SetRiseRotationValue(0f);
        SetPoleAthleteOffset(new Vector3(0, 0, 0));

    }


    //Idle Pole Hold
    public void IdleHold()
    {
        poleGrabPointR.transform.position = handR.transform.position;
        poleGrabPointR.transform.LookAt(handL.transform);
    }

    //Fix Position of Pole While being held by athlete
    public void RunUpControl()
    {
        poleGrabPointR.transform.position = handR.transform.position;

        poleGrabPointR.transform.LookAt(handL.transform);
    }


    //Switch to phase where Movement of Pole is not determined by the athlete but by the pole itself
    public void SwitchToRotationReferenceControl()
    {
        SetPoleAthleteOffset(poleGrabPointR.transform.position - athlete.transform.position);

        SetControlState(2);

        poleGrabPointR.transform.position = _poleEndInBoxPosition;
        poleGrabPointR.transform.rotation = _poleEndInBoxRotation;
    }


    //Rotate Pole in RiseUp Phase
    public void RiseRotationControl()
    {
        athlete.transform.position = rotationReference.transform.position - _poleAthleteOffset;

        _riseRotationValue += riseRotationSpeed * Time.deltaTime;

        if(_riseRotationValue < riseRotationLimit)
        {
            poleEnd.transform.Rotate(riseRotationSpeed * Time.deltaTime, 0f, 0f);
        }
        else
        {
            SetControlState(3);
        }
    }


    private void Update()
    {
        
    }
}
