using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcheryBowBehaviour : MonoBehaviour
{
    private ArcheryArrowBehaviour _archeryArrowBehaviour;
    private AthleteAnimationManagerArchery _athleteAnimationManagerArchery;
    public Animator archeryBowAnimatorController;

    public GameObject _handLBowReference;
    public GameObject _handRReference;


    private void Awake()
    {
        archeryBowAnimatorController = GetComponent<Animator>();
        _archeryArrowBehaviour = FindObjectOfType<ArcheryArrowBehaviour>();
        _athleteAnimationManagerArchery = FindObjectOfType<AthleteAnimationManagerArchery>();
    }


    void Update()
    {
        transform.position = _handLBowReference.transform.position;

        transform.LookAt(_handRReference.transform, new Vector3(0f, 90f, 0f));
    }


    public void SlingshotEnd()
    {
        _archeryArrowBehaviour.SetArrowFlying(true);
        archeryBowAnimatorController.SetBool("ArrowReleased", false);
        _athleteAnimationManagerArchery.SetAnimationPhaseCustom(1);
    }

}
