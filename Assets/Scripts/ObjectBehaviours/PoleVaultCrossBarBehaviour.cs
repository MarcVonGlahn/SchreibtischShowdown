﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoleVaultCrossBarBehaviour : MonoBehaviour
{
    private bool _heightChoosingActive;
    private bool _barKnockedOff;
    private float _crossbarHeight;
    private Rigidbody _crossbarRigidbody;
    private SceneManager _sceneManager;

    private Quaternion _crossbarOriginalRotation;
    private Vector3 _crossbarEraserOffset;

    public GameObject heightAdjustmentObject;
    public float heightAdjustmentStepValue;



    //Getter and Setter
    public bool GetBarKnockedOff()
    {
        return _barKnockedOff;
    }
    public void SetBarKnockedOff(bool newState)
    {
        _barKnockedOff = newState;
    }


    public bool GetHeightChoosingActive()
    {
        return _heightChoosingActive;
    }
    public void SetHeightChoosingActive(bool newState)
    {
        _heightChoosingActive = newState;
    }


    public float GetCrossbarHeight()
    {
        return _crossbarHeight;
    }
    public void SetCrossbarHeight(float newHeight)
    {
        _crossbarHeight = newHeight;
    }
    //-----------------


    //Reset Method
    public void ResetPoleVaultCrossbarBehaviourValues()
    {
        SetBarKnockedOff(false);
    }

    private void Awake()
    {
        _crossbarRigidbody = GetComponent<Rigidbody>();
        _sceneManager = FindObjectOfType<SceneManager>();
    }


    private void Start()
    {
        _crossbarOriginalRotation = this.transform.rotation;
        _crossbarEraserOffset = heightAdjustmentObject.transform.position - transform.position;
    }


    private void Update()
    {
        if(_sceneManager.GetCurrentScenePhase() == 2)
        {
            if(GetBarKnockedOff() == true)
            {
                
            }

            _sceneManager.SetNextScenePhase();
            StartCoroutine(MoveCrossbar());
        }

        if(_sceneManager.GetCurrentScenePhase() >= 3 && transform.position.y < (GetCrossbarHeight() - 0.1f))
        {
            SetBarKnockedOff(true);
        }
    }


    //Move Crossbar Up Sequence
    private IEnumerator MoveCrossbar()
    {
        Debug.Log("Coroutine Started");

        this.transform.rotation = _crossbarOriginalRotation;
        _crossbarRigidbody.constraints = RigidbodyConstraints.FreezeAll;

        //this.transform.position = _crossbarResetPosition;

        while (true)
        {
            yield return 0;

            if (Input.GetButtonDown("AT1"))
            {
                _sceneManager.SetCurrentScenePhase(4);
                Debug.Log("Exit Coroutine");
                _crossbarRigidbody.constraints = RigidbodyConstraints.None;

                
                yield break;
            }

            if (Input.GetAxis("DPadY") == 1f)
            {
                heightAdjustmentObject.transform.position += new Vector3(0f, heightAdjustmentStepValue, 0f);
                transform.position = heightAdjustmentObject.transform.position - _crossbarEraserOffset;
                Debug.Log("Up Button Pressed");

                yield return new WaitForSeconds(0.1f);
            }
            else if (Input.GetAxis("DPadY") == -1f)
            {
                heightAdjustmentObject.transform.position += new Vector3(0f, -(heightAdjustmentStepValue), 0f);
                transform.position = heightAdjustmentObject.transform.position - _crossbarEraserOffset;
                Debug.Log("Down Button Pressed");

                yield return new WaitForSeconds(0.1f);
            }
            SetCrossbarHeight(transform.position.y);
        }
    }
}
