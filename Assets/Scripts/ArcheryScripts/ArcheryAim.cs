using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcheryAim : MonoBehaviour
{
    private SceneManagerArchery _sceneManagerArchery;
    private ArcheryArrowBehaviour _archeryArrowBehaviour;
    private ArcheryBowBehaviour _archeryBowBehaviour;
    private WeatherControl _weatherControl;


    private bool _aimingProcessHappening;

    private float _xPositionStickNormalized;
    private float _yPositionStickNormalized;

    private Vector3 _aimStickPosition;
    private Vector3 _archeryAimReferencePosition;

    private Vector3 _vectorDistanceToAbsoluteMid;
    private float _distanceToAbsoluteMid;
    private int _scoreDecision;

    public float scoreSteps;
    public float maxDistanceToMid;


    public float crosshairPositionScale;
    public float aimReferenceEdgeScale;
    public GameObject crosshairUI;
    public GameObject archeryAimReference;


    //Getter and Setter
    public bool GetAimingProcessHappening()
    {
        return _aimingProcessHappening;
    }
    public void SetAimingProcessHappening(bool newState)
    {
        _aimingProcessHappening = newState;
    }


    public float GetXPositionStickNormalized()
    {
        return _xPositionStickNormalized;
    }
    public void SetXPositionStickNormalized(float newValue)
    {
        _xPositionStickNormalized = newValue;
    }


    public float GetYPositionStickNormalized()
    {
        return _yPositionStickNormalized;
    }
    public void SetYPositionStickNormalized(float newValue)
    {
        _yPositionStickNormalized = newValue;
    }


    public Vector3 GetAimStickPosition()
    {
        return _aimStickPosition;
    }
    public void SetAimStickPosition(float xValue, float yValue, float zValue)
    {
        Vector3 temp = new Vector3( xValue * crosshairPositionScale + (Screen.width / 2), 
                                    yValue * crosshairPositionScale + (Screen.height / 2), 
                                    zValue);
        _aimStickPosition = temp;
    }


    public int GetScoreDecision()
    {
        return _scoreDecision;
    }
    public void SetScoreDecision(int newValue)
    {
        _scoreDecision = newValue;
    }
    //--------------------


    //Here all the Aiming takes place, as in the UI Element and the Aim Reference get moved and
    //all Input is read.
    public void AimUpdate()
    {
        //Input
        if (Input.GetAxis("Vertical") != 0f && Input.GetAxis("Horizontal") != 0f)
        {
            SetXPositionStickNormalized(Input.GetAxis("Horizontal"));
            SetYPositionStickNormalized(Input.GetAxis("Vertical"));

            Debug.Log("XPosition: " + GetXPositionStickNormalized() + ", YPosition: " + GetYPositionStickNormalized());

            crosshairUI.transform.position = new Vector3(GetXPositionStickNormalized() * crosshairPositionScale + (Screen.width / 2),
                                                        GetYPositionStickNormalized() * crosshairPositionScale + (Screen.height / 2),
                                                        0f);

            SetAimStickPosition(GetXPositionStickNormalized(), GetYPositionStickNormalized(), 0f);

            archeryAimReference.transform.localPosition = new Vector3(  0f, 
                                                                        GetXPositionStickNormalized() * aimReferenceEdgeScale + _weatherControl.GetWindDirection().y * aimReferenceEdgeScale, 
                                                                        GetYPositionStickNormalized() * aimReferenceEdgeScale + _weatherControl.GetWindDirection().z * aimReferenceEdgeScale);

        }
        //No Input
        else if (Input.GetAxis("Vertical") == 0f && Input.GetAxis("Horizontal") == 0f)
        {
            crosshairUI.transform.position = new Vector3((Screen.width / 2),
                                                        (Screen.height / 2),
                                                        0f);

            SetAimStickPosition(GetXPositionStickNormalized(), GetYPositionStickNormalized(), 0f);


            archeryAimReference.transform.localPosition = new Vector3(  0f, 
                                                                        _weatherControl.GetWindDirection().y * aimReferenceEdgeScale,
                                                                        _weatherControl.GetWindDirection().z * aimReferenceEdgeScale);
        }

        _archeryArrowBehaviour.SetArrowPath();
    }


    //Decides the score of the attempt
    public void ScoringDecision(Vector3 choosenAim)
    {
        Vector3 absoluteMid = new Vector3(0f, 0f, 0f);

        _vectorDistanceToAbsoluteMid = choosenAim - absoluteMid;

        _distanceToAbsoluteMid = choosenAim.magnitude;

        for(int i = 1; i <= 10; i++)
        {
            float tempScore = new float();

            tempScore = maxDistanceToMid - (scoreSteps * i) + scoreSteps;

            if(tempScore >= _distanceToAbsoluteMid)
            {
                SetScoreDecision(i);
            }
            else if(_distanceToAbsoluteMid >= maxDistanceToMid)
            {
                SetScoreDecision(0);
            }
        }
    }


    //Checks if Aiming Input was confirmed with the right bumper by Player
    public IEnumerator CheckForAimConfirm()
    {
        if(Input.GetButtonDown("ConfirmAim"))
        {
            SetAimingProcessHappening(false);
            _sceneManagerArchery.SetNextScenePhase();
            _archeryBowBehaviour.archeryBowAnimatorController.SetBool("ArrowReleased", true);
            Debug.Log("Aim Confirmed");
            _archeryArrowBehaviour.SetArrowFlying(true);
            yield return 0;
        }
    }

    public IEnumerator WaitForEndOfFrame()
    {
        yield return new WaitForEndOfFrame();
    }



    private void Awake()
    {
        _archeryArrowBehaviour = FindObjectOfType<ArcheryArrowBehaviour>();
        _archeryBowBehaviour = FindObjectOfType<ArcheryBowBehaviour>();
        _sceneManagerArchery = FindObjectOfType<SceneManagerArchery>();
        _weatherControl = FindObjectOfType<WeatherControl>();
    }


    private void Start()
    {
        SetAimingProcessHappening(false);
    }


    void Update()
    {
        if (GetAimingProcessHappening() == true)
        {
            AimUpdate();
            ScoringDecision(archeryAimReference.transform.localPosition);
            StartCoroutine(CheckForAimConfirm());
        }
    }
}
