using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherControl : MonoBehaviour
{
    private Vector3 _windDirection;
    private float _windAngle;


    //Getter and Setter
    public Vector3 GetWindDirection()
    {
        return _windDirection;
    }
    public void SetWindDirection(Vector3 newDirection)
    {
        _windDirection = newDirection;
    }


    public float GetWindAngle()
    {
        return _windAngle;
    }
    public void SetWindAngle(float newValue)
    {
        _windAngle = -(newValue * Mathf.Rad2Deg);
    }
    //---------------------


    //Decide New Wind Direction based on Chance
    public void NewWindDirection(int round)
    {
        int countRound = 4 - round;
        float valueDivider = 1f / (float)countRound;


        float yDirection = 0f;
        float zDirection = 0f;

        for (int i = 0; i < 2; i++)
        {
            if(i == 0)
            {
                yDirection = Random.Range(-1.00f, 1.00f);
            }
            else if (i == 1)
            {
                zDirection = Random.Range(-1.00f, 1.00f);
            }
        }

        float newWindAngle = Mathf.Atan2(yDirection, zDirection);

        SetWindAngle(newWindAngle);

        SetWindDirection(new Vector3(0f, yDirection * valueDivider, zDirection * valueDivider));
    }


    private void Start()
    {
        NewWindDirection(1);
    }
}
